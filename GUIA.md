## Firebase workshop

_[Repositorio de referencia](https://bitbucket.org/edus44/workshop-firebase)_

#### Intro

* Introducion a [Firebase](https://firebase.google.com/)
* Crear un proyecto en la consola de Firebase
* Crear proyecto de Vue desde un [boilerplate](https://github.com/edus44/vue-boilerplate)
* Instalar la [libreria](https://www.npmjs.com/package/firebase) y inicializarla con la configuracion en YML [+info](https://firebase.google.com/docs/web/setup)

#### Auth

* Habilitar [autenticacion con Google](https://firebase.google.com/docs/auth/web/google-signin) de Firebase
* Añadir boton de login
* Escuchar cambios de usuario
* Añadir boton de logout
* Sincronizarlo con el store

#### Firestore

* Habilitar Firestore en Firebase
* Añadir dependencia de libreria
* [Coleciones y documentos](https://firebase.google.com/docs/firestore/data-model)
* Formulario de publicacion
* Listado de comentarios
* Entidades (DocumentSnapshot, DocumentReference)
* Borrado
* OnSnapshot
* Cambiar listado por [vuefire@next](https://github.com/vuejs/vuefire/tree/firestore)

#### Storage

* Habilitar Storage en Firebase
* Formulario de subida
* [References](https://firebase.google.com/docs/storage/web/create-reference?authuser=0)
* Link de descarga

#### Hosting

* Instalar [firebase-tools](https://www.npmjs.com/package/firebase-tools) en global
* `npm i -g firebase-tools`
* `firebase login`
* `firebase init` (Solo hosting)
* `yarn build`
* `firebase deploy`
