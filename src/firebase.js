import firebase from '@firebase/app'
import '@firebase/auth'
import '@firebase/storage'
import '@firebase/firestore'
import firebaseConfig from '@/firebaseConfig'

firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()
const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

export { db, auth, storage, googleAuthProvider }
