import { auth, googleAuthProvider } from '@/firebase'

const state = {
  user: undefined,
}

const actions = {
  init({ commit }) {
    auth.onAuthStateChanged(user => {
      if (user) commit('SET_USER', user)
      else commit('CLEAR_USER')
    })
  },
  async login() {
    await auth.signInWithPopup(googleAuthProvider)
  },
  async logout() {
    await auth.signOut()
  },
}

const mutations = {
  SET_USER(state, user) {
    state.user = {
      email: user.email,
      displayName: user.displayName,
    }
  },
  CLEAR_USER(state) {
    state.user = null
  },
}

const getters = {}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}
